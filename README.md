#Zach Pedersen

My entry for CST-307 Parallel Addition.

Installation Steps:

Download and install QTSPIM.
Download and extract main.asm program from BitBucket repository.
Launch QTSPIM and left click on Load File at the top left.
Select main program and run.
Input desired number and receive result.